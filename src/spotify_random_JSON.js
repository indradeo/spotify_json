
function tsMain(tsPlaylistGroup, tsPlaylistsJSONPath){

/*
/	This is the main working function of the task, pre-requisites for the propper running:
/   - playlist group name should be specified in the Tasker task (%tsPlaylistGroup)
/   - path to JSON file db should as well be specified in the Tasker task (%tsPlaylistsJSON)
*/
	if ( typeof tsPlaylistGroup === 'undefined' ){
		console.log( 'No group specified' );
		return false;
	}

	if ( typeof tsPlaylistsJSONPath === 'undefined' ){
		console.log( 'No path specified for JSON file' );
		return false;
	}


	// Global JS variables
	var jsJSON;
	var jsFileContent;
	var jsPlaylistGroup;
	var jsPlaylist;



	// 1. Load playlist JSON via file
	jsFileContent = readFile( tsPlaylistsJSONPath );

	jsJSON = $.parseJSON(jsFileContent);


	if ( typeof jsJSON === 'undefined' ){
		console.log( 'Encountered some problem with the file' );
		return false;
	}	


	// 2. Find playlist group
	jsPlaylistGroup = findPlaylistsByGroup(jsJSON, tsPlaylistGroup);

	if ( typeof jsPlaylistGroup === 'undefined' ){
		console.log( 'Sorry, playlist group could not be found' );
		return false;
	}	


	// 3. Select random playlist
	jsPlaylist = loadRandomPlaylist(jsPlaylistGroup.playlists);

	if ( typeof jsPlaylist === 'undefined' ){
		console.log( 'Sorry, could not identify a random playlist' );
		return false;
	}	


	// 4. Launch the selected playlist
	// for the time being will store the playlist uri in a tasker variable only..
	tsPlaylist = jsPlaylist.uri;

    if (tsPlaylist === 'undefined') {
    	console.log( 'Sorry, problem with the URI of the playlist' );
    	return false;
    };


    return tsPlaylist;

}


var tsplaylist;

tsplaylist = tsMain(tsplaylistgroup, tsplaylistsjsonpath);



