/*-----------------------------------------------------------------------------------------
*	Common JS functions																	  *
*	---------------------																  *
*	This section consists only of javascript functions that are independant of Tasker     *
------------------------------------------------------------------------------------------*/

/*
	Sample JSON structure
	-------------------------

{
  "group": [
    {
      "name": "home",
      "playlists": [
        { "title": "Chill Rock by Spotify", "uri": "spotify:user:spotify:playlist:6nIJS5m3ZyG9nNghjQNAug" },
        { "title": "Born in the 90s", "uri": "spotify:user:spotify:playlist:4NDUPAZZ1LBw9wvTOq1Mm2" }
      ]
    },
    {
      "name": "work",
      "playlists": [
        { "title": "Feelin Good", "uri": "spotify:user:spotify:playlist:1B9o7mER9kfxbmsRH9ko4z" },
      ]
    }
  ]
}

*/



function myMain(){

	var myJSON = $.parseJSON( '{"group":[{"name":"home","playlists":[{"title":"Chill Rock by Spotify","uri":"spotify:user:spotify:playlist:6nIJS5m3ZyG9nNghjQNAug"},{"title":"Born in the 90s","uri":"spotify:user:spotify:playlist:4NDUPAZZ1LBw9wvTOq1Mm2"}]},{"name":"work","playlists":[{"title":"Feelin Good","uri":"spotify:user:spotify:playlist:1B9o7mER9kfxbmsRH9ko4z"},{"title":"BStress Relief by Spotify","uri":"spotify:user:spotify:playlist:3SXtTLpEuiEgovsSM6o4bF"}]}]}');
	var playlistGroup = findPlaylistsByGroup(myJSON, "work");

	console.log(playlistGroup);

	var playlist = loadRandomPlaylist(playlistGroup.playlists);

	console.log(playlist);

}


// Try to find playlist group 
function findPlaylistsByGroup(playlistsGroupJSON, groupname){

    var playlistGroup;

	$.each( playlistsGroupJSON.group, function(key, value){
		if (value.name.toLowerCase() === groupname.toLowerCase()) {
			playlistGroup = value;
		}
	})

	return playlistGroup;

}

// Load random playlist from a group
function loadRandomPlaylist (playlists){

	var playlist = playlists[Math.floor(Math.random()*playlists.length)];

	return playlist;
}

function findPlaylistDetails(playlistsGroupJSON, playlistName){

	$.each( playlistsGroupJSON.group, function(key, group){


		$.each( group.playlists , function( index, playlist ){

			if ( playlist.title.toLowerCase() === playlistName.toLowerCase() ){

				result 			 		= {};
				result.groupname 		= group.name;
				result.title     		= playlist.title;
				result.uri       		= playlist.uri;
				result.playlistPosition = index;

				return result;
			}
		})
	})

	return result;
}

// Add a new group to JSON
function addGroup ( playlistsGroupJSON, groupName, playlistTitle, playlistUri ){
	var playlist;
	var playlists = [];
	var group = {};

	group = findPlaylistsByGroup(playlistsGroupJSON, groupName);

	console.log( playlistsGroupJSON );

	if (group === undefined) {
		playlist = {};
		group = {};

		group.name = groupName.toLowerCase();

		if (playlistTitle && playlistUri){
		    playlist.title = playlistTitle.toLowerCase();
			playlist.uri   = playlistUri;
			playlists.push(playlist);
			group.playlists = playlists;
		}

		playlistsGroupJSON.group.push(group);
	};

	return playlistsGroupJSON;

}

// Add playlist to JSON
function addPlaylist ( playlistsGroupJSON, groupName, playlistTitle, playlistUri ){

	var playlist = {};
	var group = {};

	// check if group is present
	group = findPlaylistsByGroup(playlistsGroupJSON, groupName);

	if (group === undefined){
		// group was not found, create it with the playlist details. No validation required
		addGroup ( playlistsGroupJSON, groupName, playlistTitle, playlistUri );
		console.log("here");
	} else {
		// group already present, just create a new playlist entry
		playlist.title = playlistTitle.toLowerCase();
		playlist.uri   = playlistUri;
		
		if (group.playlists === undefined){
			group.playlists = [];
		}

		group.playlists.push(playlist);
	}

	return true;
}


// Delete a given playlist without any validation whatsoever on the group
function deletePlaylist(playlistsGroupJSON, playlistTitle){

	// loop through each group 
	$.each( playlistsGroupJSON.group, function(index, group){

	  // loop through each playlist
		for( var i = 0; i < group.playlists.length; i++){;

			if( group.playlists[i].title.toLowerCase() === playlistTitle.toLowerCase()){
				group.playlists.splice(i, 1);

				// Reset current index so as not to break the loop
				i--;
			}
		}

	} )

}


// Smaple JSON for off-device testing purposes
function getSampleJSON(){

	var myJSON = $.parseJSON('{"group":[{"name":"home","playlists":[{"title":"Chill Rock by Spotify","uri":"spotify:user:spotify:playlist:6nIJS5m3ZyG9nNghjQNAug"},{"title":"Born in the 90s","uri":"spotify:user:spotify:playlist:4NDUPAZZ1LBw9wvTOq1Mm2"}]},{"name":"work","playlists":[{"title":"Feelin Good","uri":"spotify:user:spotify:playlist:1B9o7mER9kfxbmsRH9ko4z"},{"title":"Stress Relief by Spotify","uri":"spotify:user:spotify:playlist:3SXtTLpEuiEgovsSM6o4bF"}]}]}');

	return myJSON;
}


/*-----------------------------------------------------------------------------------------
*	Tasker specific functions															  *
*	--------------------------															  *
*	This is section contains only javascript functions that require tasker to work		  *
------------------------------------------------------------------------------------------*/

function saveJSONtoFile(myJSON, strPath){

	if ( myJSON === undefined || strPath === undefined){
		console.log(" problem with parameters passed in...");
		return false;
	}

	return writeFile( strPath, JSON.stringify(myJSON, undefined, 2), false);

}
